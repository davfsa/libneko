``libneko.versioning``
======================

**Version control information and statistics!**

.. inheritance-diagram:: libneko.versioning

.. automodule:: libneko.versioning
    :members:
    :inherited-members:
    :show-inheritance: