``libneko.strings``
===================

**String magic and voodoo.**

.. inheritance-diagram:: libneko.strings

.. automodule:: libneko.strings
    :members:
    :inherited-members:
    :show-inheritance:
