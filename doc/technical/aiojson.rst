``libneko.aiojson``
===================

**aiofiles + json = <3**

.. inheritance-diagram:: libneko.aiojson

.. automodule:: libneko.aiojson
    :members:
    :inherited-members:
    :show-inheritance:
