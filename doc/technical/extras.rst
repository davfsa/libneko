``libneko.extras``
==================

**Bits of code I wrote that you might find useful or fun to play with. These usually take the form of cogs or**
**extensions.**

.. inheritance-diagram:: libneko.extras.superuser libneko.extras.help

``superuser`` - exec commands
------------------------------

**Extension to allow the bot owner to execute code from Discord chat.**

.. automodule:: libneko.extras.superuser
    :members:
    :inherited-members:
    :show-inheritance:



``help`` - a help command
------------------------------

**Extension to make a snazzy help command.**

.. automodule:: libneko.extras.help
    :members:
    :inherited-members:
    :show-inheritance:

