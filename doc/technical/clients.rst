``libneko.clients``
====================

**Adds some new features to the existing Bot and AutoShardedBot classes.**

.. inheritance-diagram:: libneko.clients


``Bot``
-------

.. autoclass:: libneko.clients.Bot
    :members:
    :inherited-members:
    :show-inheritance:


``AutoShardedBot``
------------------

.. autoclass:: libneko.clients.AutoShardedBot
    :members:
    :inherited-members:
    :show-inheritance:
