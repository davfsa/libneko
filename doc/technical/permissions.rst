``libneko.permissions``
=======================

**Discord guild permissions represented as bitwise flags.**

.. inheritance-diagram:: libneko.permissions

.. automodule:: libneko.permissions
    :members:
    :inherited-members:
    :show-inheritance:
